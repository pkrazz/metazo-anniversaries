<?php

namespace App\Services;

use GuzzleHttp\Client;

class ApiService
{
    private $client;
    private $apiUrl;
    private $credentials;
    private $token;


    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->apiUrl = env('METAZO_URL');
        $this->credentials = [
            'site' => 1,
            'user' => env('METAZO_USER'),
            'password' => env('METAZO_PASSWORD'),
            'secret' => env('METAZO_SECRET'),
        ];
        $this->token = $this->retrieveToken();
    }

    /**
     * Generates an hmac based on the provided data and the secret
     * @param array $data associative array
     * @param string $secret shared secret
     * @return string sha1 hmac
     */
    private function generateChecksum(array $data, string $secret): string
    {
        ksort($data);
        $pairstr = '';
        foreach ($data as $key => $val) {
            $pairstr .= $key . $val;
        }
        return hash_hmac("SHA1", $pairstr, $secret);
    }

    private function buildParams(array $data): array
    {
        $params = $data;
        if ($this->token) {
            $params = array_merge($data, ['token' => $this->token]);
        }
        $params['checksum'] = $this->generateChecksum($params, $this->credentials['secret']);
        return $params;
    }

    public function retrieveToken(): string
    {
        $endpoint = $this->apiUrl . '/v1/authenticate';
        $params = $this->buildParams([
            'site' => $this->credentials['site'],
            'user' => $this->credentials['user'],
            'pass' => $this->credentials['password'],
        ]);

        try {
            $response = $this->client->get($endpoint, [
                'query' => $params,
            ]);

            $statusCode = $response->getStatusCode();

            if ($statusCode === 200) {
                $data = json_decode($response->getBody(), true);
                return $data['token'] ?? null;
            } else {
                throw new \Exception("Failed to retrieve token with status code: $statusCode");
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    public function explainDatatype($datatype): array
    {
        $endpoint = $this->apiUrl . '/v1/explain';
        $params = $this->buildParams([
            'datatype' => $datatype,
        ]);

        $response = $this->client->get($endpoint, [
            'query' => $params,
        ]);
        return json_decode($response->getBody(), true);
    }

    public function getEmployees(): array
    {
        $endpoint = $this->apiUrl . '/v1/list';
        $params = $this->buildParams([
            'datatype' => 'employee',
            'fields' => 'employment,birthday',
        ]);

        $response = $this->client->get($endpoint, [
            'query' => $params,
        ]);
        return json_decode($response->getBody(), true);
    }
}
