<?php

namespace App\Services;

use Carbon\Carbon;

class EmployeeService
{
    private $datatype = 'employee';
    private $apiService;

    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    public function getEmployees(): array
    {
        // used explain to find fields
        // $explain = $this->apiService->explainDatatype($this->datatype);

        // employment
        // birthday

        $employees = $this->apiService->getEmployees();
        return $employees['items'];
    }

    public function getEmployeesWithAnniversary(array $employees): array
    {
        $currentYear = now()->year;
        $anniversaries = [];

        foreach ($employees as $employee) {
            $employeeData = [
                'id' => $employee['objectid'],
                'name' => $employee['name'],
                'anniversaries' => [],
            ];

            // Employment anniversary
            $employmentYear = Carbon::parse($employee['employment'])->year;
            $employmentAnniversary = $currentYear - $employmentYear;
            if ($employmentAnniversary >= 10 && $employmentAnniversary % 10 === 0) {
                $employeeData['anniversaries'][] = "{$employmentAnniversary} years employment";
            }

            // Birthday anniversary
            $birthYear = Carbon::parse($employee['birthday'])->year;
            $birthAnniversary = $currentYear - $birthYear;
            if ($birthAnniversary >= 10 && $birthAnniversary % 10 === 0) {
                $employeeData['anniversaries'][] = "{$birthAnniversary} years birthday";
            }

            // Add only if there are anniversaries
            if (!empty($employeeData['anniversaries'])) {
                $anniversaries[] = $employeeData;
            }
        }

        return $anniversaries;
    }
}
