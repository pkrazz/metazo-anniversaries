<?php

namespace App\Http\Controllers;

use App\Services\EmployeeService;

class EmployeeController extends Controller
{
    private $employeeService;

    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function index()
    {
        $employees = $this->employeeService->getEmployees();

        $employeesWithAnniversary = $this->employeeService->getEmployeesWithAnniversary($employees);

        return $employeesWithAnniversary;
    }
}
